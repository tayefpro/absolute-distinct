package org.example;


public class Main {
    static int distinctCount(int arr[], int n)
    {
        int count = n;
        int i = 0, j = n - 1, sum = 0;

        while (i < j)
        {

            while (i != j && arr[i] == arr[i + 1])
            {
                count--;
                i++;
            }

            while (i != j && arr[j] == arr[j - 1])
            {
                count--;
                j--;
            }

            if (i == j)
                break;
            sum = arr[i] + arr[j];

            if (sum == 0)
            {

                count--;
                i++;
                j--;
            }
            else if(sum < 0)
                i++;
            else
                j--;
        }

        return count;
    }



    public static void main (String[] args) {

        int arr[] = {-1, -1, 0, 2, 2, 3, 0, 1, 5, 9};
        int n = arr.length;

        System.out.println ("Count of absolute distinct values : "+
                distinctCount(arr, n));


    }
}
